---
layout: splash
permalink: /
hidden: true
header:
  overlay_color: "#5e616c"
  overlay_image: "/assets/images/os-freelancers-header-triangle.png"
excerpt: Need help with Open Science? Get it here!
feature_row:
  - image_path: /assets/images/Verena-Heise.jpeg
    alt: "Verena"
    title: "Verena Heise"
    excerpt: "
	**Open Science Expertise**: Open Access Publishing, Open Data/ Methods, Reproducibility/ Replicability, Research Culture, Research(er) Assessment <br/><br/> 
	**Services**: Speaking, Training, Consultancy, Event Organisation, Event Moderation, Research 
    "
    url: "/experts/VerenaHeise"
    btn_class: "btn--primary"
    btn_label: "More"
  - image_path: /assets/images/Heidi-Seibold.png
    alt: "Heidi"
    title: "Heidi Seibold"
    excerpt: "
    **Open Science Expertise**: Reproducibility, Open Data, Open Source, Research Software Engineering  <br/><br/>
    **Services**: Training, Consultancy, Speaking, Event Moderation, Event Organisation
    "
    url: "/experts/HeidiSeibold"
    btn_class: "btn--primary"
    btn_label: "More"
  - image_path: /assets/images/Jo-Havemann.png
    alt: "Jo"
    title: "Jo Havemann"
    excerpt: "
    **Open Science Expertise**: Open Science Communication, Open Access Publishing, Open Project and Data Management (#beFAIRandCARE), Knowledge Transfer, Reproducibility/ Replicability, Research(er) Assessment, Global Equity/ Diversity/ Inclusivity in Research <br/><br/>
    **Services**: Speaker, Training, Consultancy, Research, Mentoring, Community building, Scholarly Editorial Services and Copy Editing"
    url: "/experts/JoHavemann"
    btn_class: "btn--primary"
    btn_label: "More"
  - image_path: /assets/images/Chris-Hartgerink.jpg
    alt: "Chris Hartgerink"
    title: "Chris Hartgerink"
    excerpt: "
    **Open Science Expertise**: Meta-research, PIDs, business development, open everything 😊 <br/><br/>
    **Services**: Facilitation (e.g., panels), hosting (e.g., journals or websites), software engineering, and more"
    url: "/experts/ChrisHartgerink"
    btn_class: "btn--primary"
    btn_label: "More"
  - image_path: /assets/images/Gracielle-Higino.png
    alt: "Gracielle"
    title: "Gracielle Higino"
    excerpt: "
	**Open Science Expertise**: Open Access Publishing, Peer Review, Open Leadership, Open Project Management, Open Data (especially in Ecology and Evolution), Reproducibility, Research Culture, science communication. <br/><br/> 
	**Services**: Speaking, Training (delivery and development), Consultancy, Event Organisation, Research, Mentorship, Community Management, Project Management, science communication strategy and production, Peer Review.
    "
    url: "/experts/GracielleHigino"
    btn_class: "btn--primary"
    btn_label: "More"
  - image_path: /assets/images/Donny-Winston.jpg
    alt: "Donny"
    title: "Donny Winston"
    excerpt: "
	**Open Science Expertise**: Machine Actionability (FAIR Principles) as an engine for Open-able Research. Open Access Publishing, Open Data/ Methods, Open Source, Open Educational Resources, Reproducibility/ Replicability, Research Software Engineering. <br/><br/> 
	**Services**: Speaker, Training, Consultancy.
    "
    url: "/experts/DonnyWinston"
    btn_class: "btn--primary"
    btn_label: "More"    

  - image_path: /assets/images/Susann-Auer.jpeg
    alt: "Susann"
    title: "Susann Auer"
    excerpt: "
      **Open Science expertise**:
      Open Access Publishing, Open Data/ Methods, Open Educational Resources,
      Reproducibility/ Replicability, Research Culture, Diversity/ Inclusivity in
      Research, Reproducibility in Reporting for Scholary Publications <br/><br/> 
      **Services**:
      Speaker, Training, Consultancy, Peer Review
      "
    url: "experts/SusannAuer"
    btn_class: "btn--primary"
    btn_label: "More"
  
  - image_path: /assets/images/Peter-Schmidt.jpeg
    alt: "Peter"
    title: "Peter Schmidt"
    excerpt: "
      **Open Science expertise**:
      Open Access Publishing, Open Source, Reproducibility, Research Software Engineering, Research Culture  <br/><br/> 
      **Services**:
      Podcast production and hosting for public engagement in science/engineering and for training/education
      "
    url: "experts/PeterSchmidt"
    btn_class: "btn--primary"
    btn_label: "More"
    
  - image_path: /assets/images/Michael-Markie.jpg
    alt: "Michael"
    title: "Michael Markie"
    excerpt: "
      **Open Science Expertise**: Open Access Publishing, Open Data, Open Peer Review, Open Infrastructure, Community Building, Open Science Publishing Workflows and Operations and Open Science Policy Development <br/><br/>
      **Services**: Speaker, Consultancy, Event Organisation, Mentorship, Community Management, Project Management
      "
    url: "experts/MichaelMarkie"
    btn_class: "btn--primary"
    btn_label: "More"
---

# Our experts

{% include feature_row %}
