---
title: "Susann Auer"
excerpt: "Reproducibility for everyone"
header:
  teaser: assets/images/Susann-Auer.jpeg
sidebar:
  - title: "Homepage"
    image: assets/images/Susann-Auer.jpeg
    image_alt: "Susann"
    text: "[www.repro4everyone.org](https://www.repro4everyone.org/)"
  - title: "Contact"
    text: "[Susann.Auer@gmail.com](mailto:Susann.Auer@gmail.com)"
  - title: "Languages"                       
    text: "German, English"
---
### Open Science expertise: 
Open Access Publishing, Open Data/ Methods, Open Educational Resources, Reproducibility/ Replicability, Research Culture, Diversity/ Inclusivity in Research, Reproducibility in Reporting for Scholary Publications

### Services: 
Speaker, Training, Consultancy, Peer Review

### Research background: 
PhD in Biology, Plant Sciences, Meta-Research, Communication and Media Science

### Speaker expertise: 
I offer talks on Open Science, Reproducibility in Research and community learning in an engaging atmosphere. I am regularly booked as a speaker for Science Communication on mainly biological topics e.g. in a Senior Academy, at the Open Science Night or educational centers.

### Training expertise: 
My modular workshops in reproducibility contain training in Data Management, good method writing, data visualisation and more. These workshops are suited for all audiences at beginner and intermediate experience levels including early career researchers, PIs, technical personnel. I adjust and develop my training materials according to my audience and the demands of the event.  


City: Dresden <br/> 
Country: Germany <br/>
<!--Please delete/ change the following as appropriate-->
&#x2611; Happy to offer services virtually <br/>
&#x2611; Happy to travel in Germany and Europe <br/>
{: .notice}




