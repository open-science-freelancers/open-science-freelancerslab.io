---
title: "Donny Winston"
excerpt: "Helping your research run on clean data"
header:
  teaser: assets/images/Donny-Winston.jpg
sidebar:
  - title: "Homepage"
    image: assets/images/Donny-Winston.jpg
    image_alt: "Donny"
    text: "[donnywinston.com](https://donnywinston.com)"
  - title: "Contact"
    text: "[donny@polyneme.xyz](mailto:donny@polyneme.xyz)"
  - title: "Languages"                       
    text: "English"
---
### Open Science expertise: 
<!--PLEASE SELECT THE ONES THAT APPLY TO YOU:-->
Open Access Publishing, Open Data/ Methods, Open Source, Open Educational Resources, Reproducibility/ Replicability, Research Software Engineering

### Services: 
<!--PLEASE SELECT THE SERVICES THAT YOU OFFER:-->
Speaker, Training, Consultancy

### Research background: 
<!--PLEASE ENTER YOUR AREA OF RESEARCH (e.g. Biomedical Sciences)-->
Electrical Engineering (Materials and Devices), Nanofabrication, Computational Materials Science and Engineering

### Speaker expertise: 
<!--Please enter free text on the talks that you offer or delete if you do not offer talks-->

- A Walking Tour of the 15 FAIR Principles

- The Structure and Interpretation of FAIR Implementation Profiles

- How to FAIRify a Dataset

- How and Why to Adopt Continuous and Ubiquitous FAIR Infrastructure

### Training expertise: 
<!--Please enter free text on the training that you offer or delete if you do not offer training-->

- Introduction to Linked Data using Python and JSON-Document-oriented Databases such as MongoDB ([open courseware](https://github.com/polyneme/intro-linkeddata-mongo-python))

- Design and Deployment of ARK-scheme HTTP URIs as Globally Unique, Persistent, Resolvable Identifiers.

### Consultancy expertise: 
<!--Please enter free text on the consultancy services that you offer or delete if you do not offer consultancy-->

- Strategy call: Get clarity on aspects of FAIR that are blocking your implementation progress so that you can de-risk your approach.

- FIP Inventory + Roadmap: Get an unambiguous plan of action you can hand off to software developers and data engineers.

- Dataset FAIRification PoC: Treat your favorite dataset, or make good on a Data Management Plan (DMP) claim. I give you specifics to apply your FAIR Implementation Profile (FIP) — clarifying and sequencing the steps to materialize and deploy resources — to your particular dataset, plus Proof of Concept (PoC) code to get you started.

- Infrastructure Roadmap: Get clear on your adoption/implementation and integration of FAIR-enabling resources — identifier services, metadata schemas, registries, preservation policies, controlled-vocabulary stewardship, usage licenses, etc. — throughout your projects' research lifecycles. I design and deliver a roadmap to continuous and ubiquitous FAIR data infrastructure for your lab.

- DMP Rescue: Worry less. Three months of oversight for FAIR Data Management Plan (DMP) fulfillment, aka "DMP Rescue". I will proactively help ensure your DMP is fulfilled. I check in on communication channels and bug/issue trackers. Oversight service may include delivery of task-relevant training workshops.

- Infra Roadmap + PoC: When you want a concrete basis for iteration. The Infrastructure Roadmap service, plus a Proof of Concept (PoC) system implementation to get you started.

- Infra Roadmap + PoC + Oversight: When you need proactive surveying of ground truth to be confident the plan is being honored.

### Additional info: 
I also host a podcast, [Machine-Centric Science](https://podcast.polyneme.xyz/); blog on my [personal website](https://donnywinston.com); and co-organize the [FAIR Points](https://www.fairpoints.org/) effort with [Sara El-Gebali](https://twitter.com/yalahowy) and [Chris Erdmann](https://twitter.com/libcce). <br/><br/> You can view current fees (spoiler: they are Group 1 element Z numbers in 10<sup>3</sup> USD) for my productized services and book a free 10-minute consultation at [https://polyneme.xyz/offerings](https://polyneme.xyz/offerings).

City: New York <br/> 
Country: United States <br/>
<!--Please delete/ change the following as appropriate-->
&#x2611; Happy to offer services virtually <br/>
{: .notice}



