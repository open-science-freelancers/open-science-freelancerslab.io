---
title: "Michael Markie"
excerpt: "YOUR SUPER SHORT DESCRIPTION"
header:
  teaser: assets/images/Michael-Markie.jpg
sidebar:
  - image: assets/images/Michael-Markie.jpg
    image_alt: "Michael"
  - title: "Contact"
    text: "[michael.l.markie@gmail.com](mailto:michael.l.markie@gmail.com)"
  - title: "Languages"                       
    text: "English"
---

### Open Science expertise: 
Open Access Publishing, Open Data, Open Peer Review, Open Infrastructure, Community Building, Open Science Publishing Workflows and Operations and Open Science Policy Development.

### Services: 
Speaker, Consultancy, Event Organisation, Mentorship, Community Management, Project Management

### Research background: 
Chemical Biology

### Speaker expertise: 
Keynotes, panel moderation and talks on all aspects of the open science research lifecycle.


### Consultancy expertise: 
Open Science strategy development and implementation for funders, societies, institutions and publishers. Building and connecting open science communities interested in embracing collaborative open research by educating and demonstrating how open science practices, tools, metrics and incentives can make meaningful change.



### Additional info: 
Please feel free to contact me for an informal chat about potential projects.

City: London <br/> 
Country: UK <br/>
&#x2611; Happy to offer services virtually <br/>
&#x2611; Happy to travel worldwide <br/>
{: .notice}



